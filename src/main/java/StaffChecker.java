import java.util.Scanner;

public class StaffChecker {
    public static void main(String[] args) {
        Scanner scan= new Scanner(System.in);
        Student s1= new Student(1, "Tobey Maguire");
        Student s2= new Student(2, "Andrew Garfield");
        Student s3= new Student(3, "Tom Holland");

        StudentsData data= new StudentsData();
        data.addStudent(s1);
        data.addStudent(s2);

        System.out.println("Are you an admin?");
        int option, rollno, newRollNo;
        String name, newName;
        String answer= scan.next();
        if(answer.equalsIgnoreCase("Yes")){
            while(true){
                System.out.print("\n1. Add Student\n2. Remove Student\n3. Update Student" +
                        "\n4. View the students\n5. Exit\n Enter the option: ");
                option= scan.nextInt();
                switch(option){
                    case 1: data.addStudent(s3);
                            break;
                    case 2: data.removeStudent(2);
                            break;
                    case 3: data.updateStudent(3,4,"MJ");
                            break;
                    case 4: data.viewStudents();
                            break;
                    case 5: System.exit(1);
                }
            }
        }
        else{
            System.out.println("Do you want to view the data?");
            answer= scan.next();
            if(answer.equalsIgnoreCase("Yes"))
                data.viewStudents();
        }

    }
}

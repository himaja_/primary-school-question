import java.util.ArrayList;

public class StudentsData {
    private ArrayList<Student> data;

    public StudentsData() {
        data = new ArrayList<>();
    }

    public void addStudent(Student s) {
        this.data.add(s);
    }

    public Student getStudent(int rollno) {
        for (Student i : this.data) {
            if (i.getRollno() == rollno)
                return i;
        }
        System.out.println("Student with the given Roll Number is not found");
        return null;
    }

    public void removeStudent(int rollno) {
        for (Student i : this.data) {
            if (i.getRollno() == rollno) {
                this.data.remove(i);
                break;
            }
        }
    }

    public void updateStudent(int rollno, int newRollno, String newName) {
        for (int i = 0; i < this.data.size(); i++) {
            if (this.data.get(i).getRollno() == rollno) {
                Student newStudent = new Student(newRollno, newName);
                this.data.set(i, newStudent);
                break;
            }
        }
    }

    public void viewStudents() {
        System.out.println();
        for (Student i : this.data) {
            System.out.println(i.getRollno() + " " + i.getName());
        }
    }

}
